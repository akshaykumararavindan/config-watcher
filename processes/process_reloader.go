package processes

import (
	"fmt"
	"syscall"

	"go.uber.org/zap"
)

func ReloadProcess(logger *zap.Logger, process string, signal syscall.Signal) error {
	pid, err := FindPID(logger, process)
	if err != nil {
		return err
	}

	// @TODO: Make platform specific kill calls
	err = syscall.Kill(pid, signal)
	if err != nil {
		return fmt.Errorf("could not send signal: %v", err)
	}

	logger.Info(fmt.Sprintf("signal %s sent to %s (pid: %d)", signal, process, pid))
	return nil
}

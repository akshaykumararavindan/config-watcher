package processes

import (
	"fmt"

	"github.com/mitchellh/go-ps"
	"go.uber.org/zap"
)

func FindPID(logger *zap.Logger, process string) (int, error) {
	processes, err := ps.Processes()
	if err != nil {
		return -1, fmt.Errorf("failed to list processes: %v", err)
	}

	for _, p := range processes {
		if p.Executable() == process {
			logger.Info(fmt.Sprintf("found executable %s (pid: %d)", p.Executable(), p.Pid()))
			return p.Pid(), nil
		}
	}

	return -1, fmt.Errorf("no process matching %s found", process)
}

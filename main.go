package main

import (
	"gitlab.com/akshaykumararavindan/config-watcher/watcher/watcher"

	"go.uber.org/zap"
)

type Logger struct {
	Log *zap.Logger
}

func main() {
	// Initialize values
	logger := &Logger{}

	var err error
	logger.Log, err = zap.NewProduction()
	if err != nil {
		return
	}

	defer logger.Log.Sync()

	watcher.RunWatcher(logger.Log)
}

module gitlab.com/akshaykumararavindan/config-watcher/watcher

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/mitchellh/go-ps v1.0.0
	go.uber.org/zap v1.21.0
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e
)

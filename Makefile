.phony: build run

PROCESS_NAME="proftpd"
RELOAD_SIGNAL="SIGHUP"
CONFIG_DIR=$(shell pwd)

build:
	go build -o build/watcher

run:
	CONFIG_DIR=$(CONFIG_DIR) PROCESS_NAME=$(PROCESS_NAME) RELOAD_SIGNAL=$(RELOAD_SIGNAL) go run ./main.go

dockerize:
	docker build -t luciferreficul/config-watcher:latest .
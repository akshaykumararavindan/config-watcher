package helpers

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"go.uber.org/zap"
)

type FileHelpersStruct struct {
	WatchDirectories []string
}

func (fh *FileHelpersStruct) FileHelpers(logger *zap.Logger) []string {
	configDir := os.Getenv("CONFIG_DIR")

	if dir, err := fileHelpers(logger, configDir); err != nil {
		logger.Error(fmt.Sprintf("%v", err))
		return nil
	} else {
		return dir
	}
}

func fileHelpers(logger *zap.Logger, path string) ([]string, error) {
	fhStruct := &FileHelpersStruct{}

	watchPaths := fhStruct.WatchDirectories

	callbackFn := func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() && !strings.Contains(path, "vendor") && !strings.Contains(path, ".git") {
			watchPaths = append(watchPaths, path)
		}

		return err
	}

	if path == "" {
		logger.Error("mandatory env var CONFIG_DIR is empty, exiting")
		return nil, nil
	} else {
		err := filepath.WalkDir(path, callbackFn)

		if err != nil {
			logger.Error(fmt.Sprintf("%v - %s", err, path))
			return nil, err
		}

		return watchPaths, nil
	}
}

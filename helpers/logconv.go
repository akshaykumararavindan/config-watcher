package helpers

import "fmt"

func LogConv(msg string, val interface{}) string {
	if msg == "" {
		return fmt.Sprintf("%v", val)
	} else {
		return fmt.Sprintf("%s - %v", msg, val)
	}
}

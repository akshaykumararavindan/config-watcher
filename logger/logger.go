package logger

import (
	"go.uber.org/zap"
)

var cfg zap.Config

type Logger struct {
	logger *zap.Logger
}

func LoggerInit() {

	loggerInstance, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}

	logger := &Logger{logger: loggerInstance}

	defer logger.logger.Sync()
}

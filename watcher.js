import { EventEmitter } from "events";
import { watchFile, rename, readdir } from "fs";
import process, { kill } from "process";
import { exec } from "child_process";

const watchDir = "./",
  processedDir = "./done";

/*Let's extend events.EventEmitter in order to be able
  to emit and listen for event*/

class Watcher extends EventEmitter {
  constructor(watchDir, processedDir) {
    super();
    this.watchDir = watchDir;
    this.processedDir = processedDir;
  }

  /* Cycles through directory and process any file
  found emitting a process event for each one*/
  watch() {
    const watcher = this;
    readdir(this.watchDir, function (err, files) {
      if (err) throw err;
      for (let index in files) {
        watcher.emit("process", files[index]);
      }
    });
  }

  /* Start the directory monitoring
  leveraging Node's watchFile */
  start() {
    var watcher = this;
    watchFile(watchDir, function () {
      watcher.watch();
    });
  }
}

/* Let's instantiate our Watcher object
 passing to the constructor our folders path */

let watcher = new Watcher(watchDir, processedDir);

/*Let's use the on method inherited from
event emitter class to listen for process
events and move files from source folder
to destination*/

watcher.on("process", function process(file) {
  var yourPID = "12648";

  exec("tasklist", function (err, stdout, stderr) {
    console.log(stdout.indexOf("com.docker.backend.exe"));
    var lines = stdout.toString().split("\n");
    var results = new Array();

    kill(yourPID, "SIGINT");
    // console.log(lines);

    lines.forEach(function (line) {
      var parts = line.split("=");

      parts.forEach(function (items) {
        if (items.toString().indexOf(yourPID) > -1) {
          console.log(
            items.toString().substring(0, items.toString().indexOf(yourPID))
          );
        }
      });
    });
  });
});

/*Start it!!!*/

watcher.start();

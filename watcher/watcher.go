package watcher

import (
	"fmt"
	"os"
	"syscall"

	"gitlab.com/akshaykumararavindan/config-watcher/watcher/helpers"
	"gitlab.com/akshaykumararavindan/config-watcher/watcher/processes"

	"go.uber.org/zap"

	"github.com/fsnotify/fsnotify"
	"golang.org/x/sys/unix"
)

func RunWatcher(logger *zap.Logger) {
	// Flush the logger buffer

	processName := os.Getenv("PROCESS_NAME")
	if processName == "" {
		logger.Error("mandatory env var PROCESS_NAME is empty, exiting")
	}

	var reloadSignal syscall.Signal
	reloadSignalStr := os.Getenv("RELOAD_SIGNAL")
	if reloadSignalStr == "" {
		logger.Info("RELOAD_SIGNAL is empty, defaulting to SIGHUP")
		reloadSignal = syscall.SIGHUP
	} else {
		reloadSignal = unix.SignalNum(reloadSignalStr)
		if reloadSignal == 0 {
			logger.Error(fmt.Sprintf("cannot find signal for RELOAD_SIGNAL: %s", reloadSignalStr))
		}
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logger.Error(fmt.Sprintf("A watcher instance could not be initialized %v", err))
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}

				if event.Op&fsnotify.Chmod != fsnotify.Chmod {
					logger.Info(fmt.Sprintf("modified file: %v", event.Name))
					err := processes.ReloadProcess(logger, processName, reloadSignal)
					if err != nil {
						logger.Error(helpers.LogConv("", err))
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				logger.Error(fmt.Sprintf("%v", err))
			}
		}
	}()

	fileHelper := &helpers.FileHelpersStruct{}

	paths := fileHelper.FileHelpers(logger)

	for _, dir := range paths {
		logger.Info(helpers.LogConv("Watching path: %s", dir))
		err = watcher.Add(dir)
		if err != nil {
			logger.Error(fmt.Sprintf("Path %s - %v", dir, err))
		}
	}

	<-done
}
